-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Jeu 20 Juillet 2017 à 04:47
-- Version du serveur :  5.6.35
-- Version de PHP :  7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `cv_maker`
--

-- --------------------------------------------------------

--
-- Structure de la table `experience`
--

CREATE DATABASE cv_maker;
USE cv_maker;

CREATE TABLE `experience` (
  `mois_debut` varchar(30) NOT NULL,
  `annee_debut` varchar(30) NOT NULL,
  `mois_fin` varchar(30) DEFAULT NULL,
  `annee_fin` varchar(30) DEFAULT NULL,
  `duree` int(11) DEFAULT NULL,
  `type` text NOT NULL,
  `lieu` text NOT NULL,
  `description` text,
  `extras` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `formation`
--

CREATE TABLE `formation` (
  `annee_debut` varchar(30) NOT NULL,
  `annee_fin` varchar(30) NOT NULL,
  `duree` int(11) NOT NULL,
  `nom_diplome` text NOT NULL,
  `lieu` text NOT NULL,
  `extras` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `infos_perso`
--

CREATE TABLE `infos_perso` (
  `prenom` varchar(30) NOT NULL,
  `nom` varchar(30) NOT NULL,
  `titre` text NOT NULL,
  `adresse` text NOT NULL,
  `pays` text NOT NULL,
  `ville` text NOT NULL,
  `email` varchar(30) NOT NULL,
  `numtel` varchar(30) NOT NULL,
  `age` varchar(30) DEFAULT NULL,
  `situation_matr` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `langue`
--

CREATE TABLE `langue` (
  `nom` varchar(30) NOT NULL,
  `description` varchar(30) NOT NULL,
  `extras` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `loisir`
--

CREATE TABLE `loisir` (
  `nom` text NOT NULL,
  `extras` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `langue`
--
ALTER TABLE `langue`
  ADD UNIQUE KEY `nom` (`nom`);
