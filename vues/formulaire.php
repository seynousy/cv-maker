<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>Formulaire de création de CV</title>
        <meta charset="UTF-8">
        <link href="../style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <form class="infos-perso" method="post" action="../controllers/submit_form_infos.php" >
            <fieldset>
                <legend>Informations personnelles</legend>
                <p>
                    <input type="text" name="prenom" placeholder="Prénom(s)">
                </p>
                <p>
                    <input type="text" name="nom" placeholder="NOM">
                </p>
                <p>
                    <input type="text" name="titre" placeholder="Titre">
                </p>
                <p>
                    <input type="text" name="adresse" placeholder="Adresse">
                </p>
                <p>
                    <input type="text" name="pays" placeholder="Pays">
                </p>
                <p>
                    <input type="text" name="ville" placeholder="Ville">
                </p>
                <p>
                    <input type="email" name="email" placeholder="Email">
                </p>
                <p>
                    <input type="text" name="numtel" placeholder="Numéro de téléphone">
                </p>
                <p>
                    <input type="number" name="age" placeholder="Age">
                </p>
                <input class="btn" type="submit" name="vailder-infos-perso" value="Valider les informations personnelles">
            </fieldset>
        </form>
        <form class="formations" method="post" action="../controllers/submit_form_formations.php">
            <fieldset>
                <legend>Diplôme et Études</legend>
                <div id="formations">
                    <p>
                        <input type="text" placeholder="Année de début" name="date-debut-form">
                    </p>
                    <p>
                        <input type="text" placeholder="Année de fin" name="date-fin-form">
                    </p>
                    <p>
                    <input type="number" placeholder="Durée(Mois)" name="duree-form">
                    </p>
                    <p>
                        <input type="text" placeholder="Nom du diplôme" name="nom-diplome">
                    </p>
                        <input type="text" placeholder="Lieu" name="lieu-form">
                    </p>
                    <p>
                        <input type="text" placeholder="Extras" name="extras-form">
                    </p>
                    <p>
                        <input class="btn" type="submit" name="ajouter-form" value="Ajouter Formation">
                    </p>
                </div>
            </fieldset>
        </form>
        <form class="experience" method="post" action="../controllers/submit_form_exps.php">
            <fieldset>
                <legend>Expériences Professionnelles</legend>
                <div id="experiences">
                    <p>
                        <select name="mois-debut-exp">
                            <option value="">Mois de début</option>
                            <option value="janvier">Janvier</option>
                            <option value="fevrier">Fevrier</option>
                            <option value="mars">Mars</option>
                            <option value="avril">Avril</option>
                            <option value="mai">Mai</option>
                            <option value="juin">Juin</option>
                            <option value="juillet">Juillet</option>
                            <option value="aout">Août</option>
                            <option value="septembre">Septembre</option>
                            <option value="octobre">Octobre</option>
                            <option value="novembre">Novembre</option>
                            <option value="decembre">Décembre</option>
                        </select>
                    </p>
                    <p>
                        <input type="text" placeholder="Année de début" name="date-debut-exp">
                    </p>
                    <p>
                        <select name="mois-fin-exp">
                            <option value="">Mois de début</option>
                            <option value="janvier">Janvier</option>
                            <option value="fevrier">Fevrier</option>
                            <option value="mars">Mars</option>
                            <option value="avril">Avril</option>
                            <option value="mai">Mai</option>
                            <option value="juin">Juin</option>
                            <option value="juillet">Juillet</option>
                            <option value="aout">Août</option>
                            <option value="septembre">Septembre</option>
                            <option value="octobre">Octobre</option>
                            <option value="novembre">Novembre</option>
                            <option value="decembre">Décembre</option>
                        </select>
                    </P>
                    <p>
                        <input type="text" placeholder="Année de fin" name="date-fin-exp">
                    </p>
                    <p>
                        <input type="text" placeholder="Description" name="description">
                    </p>
                    <p>
                        <input type="text" placeholder="Lieu" name="lieu-exp">
                    </p>
                    <p>
                        <input type="number" placeholder="Durée(Mois)" name="duree-exp">
                    </p>
                    <p>
                        <input type="text" placeholder="Type" name="type-exp">
                    </p>
                    <p>
                        <input type="text" placeholder="Extras" name="extras-exp">
                    </p>
                    <p>
                        <input class="btn" type="submit" name="ajouter-exp" value="Ajouter Expérience">
                    </p>
                </div>
            </fieldset>
        </form>
        <!--<form method="post" action="../controllers/submit_form_comp.php">
            <fieldset>
                <legend>Compétences</legend>
                <div id="competences">
                    <input type="text" placeholder="Catégorie" name="nom-langue">
                    <input type="text" placeholder="Extras" name="extras-langue">
                </div>
                <input type="button" name="ajouter-categorie" value="Ajouter" onclick="ajouterComp()">
            </fieldset>
        </form>-->
        <form class="comp-lang" method="post" action="../controllers/submit_form_comp_ling.php">
            <fieldset>
                <legend>Compétences Linguistiques</legend>
                <div id="langues">
                    <p>
                        <input type="text" placeholder="Langue" name="nom-langue">
                    </p>
                    <p>
                        <input type="text" placeholder="Description" name="desc-langue">
                    </p>
                    <p>
                        <input type="text" placeholder="Extras" name="extras-langue">
                    </p>
                    <p>
                        <input class="btn" type="submit" name="ajouter-langue" value="Ajouter Langue">
                    </p>
                </div>
            </fieldset>
        </form>
        <form class="loisirs" method="post" action="../controllers/submit_form_loisirs.php">
            <fieldset>
                <legend>Loisirs</legend>
                <div id="loisirs">
                    <p>
                        <input type="text" placeholder="Loisir" name="nom-loisir">
                    </p>
                    <p>
                        <input type="text" placeholder="Extras" name="extras-loisir">
                    </p>
                    <p>
                        <input class="btn" type="submit" name="ajouter-loisir" value="Ajouter Loisir">
                    </p>
                </div>
            </fieldset>
        </form>
        <form method="post" action="../vues/cv.php">
            <input class="btn" type="submit" name="sumbit-button" value="Visualiser mon CV">
        </form>
        <form method="post" action="../controllers/init_db.php">
            <input class="btn" type="submit" name="init-db" value="Réinitialiser">
        </form>
    </body>
</html>