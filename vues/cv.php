<?
    require('../models/manip_bdd.php');
    $infos = recup_donnee("infos_perso");
    $formations = recup_donnee("formation");
    $experiences = recup_donnee("experience");
    $langues = recup_donnee("langue");
    $loisirs = recup_donnee("loisir");
?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>CV Généré</title>
        <meta charset="UTF-8">
        <link href="../style_cv.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="entete">
            <div class="infos-persos-part1">
                <h1><? echo $infos[0][0] ?></h1>
                <h1><? echo $infos[0][1] ?></h1>
                <h4><i><? echo $infos[0][2] ?></i></h4>
            </div>
            <div class="infos-persos-part2">
                <h5><? echo $infos[0][3] ?></h5>
                <h5><? echo $infos[0][4] ?></h5>
                <h5><? echo $infos[0][5] ?></h5>
                <h5><? echo $infos[0][6] ?></h5>
                <h5><? echo $infos[0][7] ?></h5>
                <h5><? echo $infos[0][8] ?></h5>
            </div>
        </div>
        <div class="corps">
            <div class="section-soulignee">
                <div class="trait"></div>
                <h3 class="titre-section">Diplôme et experiences</h3>
                <div class="contenu-formations">
                    <?
                        for ($i=0; $i < count($formations); $i++) { 
                    ?>
                    <p>
                        <span class="dates"><? echo $formations[$i][0] ?> - <? echo $formations[$i][1] ?></span>
                        <span><b><? echo $formations[$i][3] ?>, </b></span>
                        <span><em><? echo $formations[$i][4] ?></em>, </span>
                        <span><? echo $formations[$i][5] ?></span>
                    </p>
                    <?
                        }
                    ?>

                </div>
            </div>
            <div class="section-soulignee">
                <div class="trait"></div>
                <h3 class="titre-section">Expériences Professionnelles</h3>
                <div class="contenu-experiences">
                    <?
                        for ($j=0; $j < count($experiences); $j++) { 
                    ?>
                    <p>
                        <span class="dates"><span><? echo $experiences[$j][0].' '.$experiences[$j][1] ?> - <? echo $experiences[$j][2].''.$experiences[$j][3] ?></span></span>
                        <span><? echo $experiences[$j][5] ?>: </span>
                        <span><b><? echo $experiences[$j][7] ?></b>, </span>
                        <span><em><? echo $experiences[$j][6] ?></em>, </span>
                        <span>Durée: <? echo $experiences[$j][4] ?> mois</span>
                    </p>

                    <?
                        }
                    ?>
                </div>
            </div>
            <!--<div class="section-soulignee">
                <div class="trait"></div>
                <h3 class="titre-section">Compétences</h3>
            </div>-->
            <div class="section-soulignee">
                <div class="trait"></div>
                <h3 class="titre-section">Langues et Loisirs</h3>
                <p>
                    <span class="dates">Langues</span>
                    <p>
                        <span>Francais: </span><span>Natif</span>
                    </p>
                    <p>
                        <span>Anglais: </span><span>Expert</span><span>Tuffle Acquéri</span>
                    </p>
                </p>
                <p>
                    <span class="dates">Loisirs</span>
                    <p>
                        <span>Lecture</span>
                    </p>
                    <p>
                        <span>BasketBall</span><span>, Coupe Nationale du Sénégal</span>
                    </p>
                </p>
            </div>
        </div>
    </body>
</html>