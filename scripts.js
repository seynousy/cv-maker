function ajouterExperience() {
    var experiences = document.getElementById('experiences')
    var blocExperience = document.createElement('div')
    blocExperience.setAttribute('class', 'bloc-experience')
    
    var elements = []

    elements.push(
    '<select id="mois-debut-exp">'+
        '<option value="">Mois de début</option>'+
        '<option value="janvier">Janvier</option>'+
        '<option value="fevrier">Fevrier</option>'+
        '<option value="mars">Mars</option>'+
        '<option value="avril">Avril</option>'+
        '<option value="mai">Mai</option>'+
        '<option value="juin">Juin</option>'+
        '<option value="juillet">Juillet</option>'+
        '<option value="aout">Août</option>'+
        '<option value="septembre">Septembre</option>'+
        '<option value="octobre">Octobre</option>'+
        '<option value="novembre>Novembre</option>'+
        '<option value="decembre">Décembre</option>'+
    '</select>');
    elements.push('<input style="margin: 5px" type=text" placeholder="Année de début" name="date-debut-exp">')
    elements.push(
    '<select id="mois-debut-exp">'+
        '<option value="">Mois de début</option>'+
        '<option value="janvier">Janvier</option>'+
        '<option value="fevrier">Fevrier</option>'+
        '<option value="mars">Mars</option>'+
        '<option value="avril">Avril</option>'+
        '<option value="mai">Mai</option>'+
        '<option value="juin">Juin</option>'+
        '<option value="juillet">Juillet</option>'+
        '<option value="aout">Août</option>'+
        '<option value="septembre">Septembre</option>'+
        '<option value="octobre">Octobre</option>'+
        '<option value="novembre>Novembre</option>'+
        '<option value="decembre">Décembre</option>'+
    '</select>');
    elements.push('<input style="margin: 5px" type=text" placeholder="Année de fin" name="date-fin-exp">')


    elements.push('<input style="margin: 5px" type="text" placeholder="Description" name="description">')
    elements.push('<input style="margin: 5px" type="text" placeholder="Lieu" name="lieu-exp">')
    elements.push('<input style="margin: 5px" type="number" placeholder="Durée(Mois)" name="duree-exp">')
    elements.push('<input style="margin: 5px" type="text" placeholder="Extras" name="extras-exp">')

    blocExperience.innerHTML = elements.join('')
    experiences.appendChild(blocExperience)
}

function ajouterFormation() {
    var formations = document.getElementById('formations')
    var blocFormation = document.createElement('div')
    blocFormation.setAttribute('class', 'bloc-formation')
    
    var elements = []

    elements.push('<input style="margin: 5px" type=text" placeholder="Année de début" name="date-debut-form">')
    elements.push('<input style="margin: 5px" type=text" placeholder="Année de fin" name="date-fin-form">')
    elements.push('<input style="margin: 5px" type="number" placeholder="Durée(Mois)" name="duree-form">')
    elements.push('<input style="margin: 5px" type="text" placeholder="Nom du diplôme" name="nom-diplome">')
    elements.push('<input style="margin: 5px" type="text" placeholder="Lieu" name="lieu-form">')
    elements.push('<input style="margin: 5px" type="text" placeholder="Extras" name="extras-form">')

    blocFormation.innerHTML = elements.join('')
    formations.appendChild(blocFormation)
}

function ajouterLangue() {
    var loisirs = document.getElementById('langues')
    var blocLangue = document.createElement('div')
    blocLangue.setAttribute('class', 'bloc-langue')

    var elements = []
    elements.push('<input style="margin: 5px" type=text" placeholder="Langue" name="nom-langue">')
    elements.push('<input style="margin: 5px" type=text" placeholder="Extras" name="extras-langue">')

    blocLangue.innerHTML = elements.join('')
    langues.appendChild(blocLangue)
}

function ajouterLoisir() {
    var loisirs = document.getElementById('loisirs')
    var blocLoisir = document.createElement('div')
    blocLoisir.setAttribute('class', 'bloc-loisir')

    var elements = []
    elements.push('<input style="margin: 5px" type=text" placeholder="Loisir" name="nom-loisir">')
    elements.push('<input style="margin: 5px" type=text" placeholder="Extras" name="extras-loisir">')

    blocLoisir.innerHTML = elements.join('')
    loisirs.appendChild(blocLoisir)
}