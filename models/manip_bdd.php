<?
    // Connexion à la base de données
    function connexion_bdd(){
		try {
			$bdd = new PDO('mysql:host=localhost;dbname=cv_maker', 'root', 'root'); // changer le mot de passe par le votre ou laissez le vide si vous en avez pas //
			$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch (Exception $e) {
			die('Erreur de connection de la base de donnees: '.$e->getMessage());
		}
		return $bdd;
	}

	function vider($nom_table){
		$bdd = connexion_bdd();
		try {
			$req = $bdd->prepare('DELETE FROM '.$nom_table);
			$req->execute();
			return $nom_table;
			$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
	}

    // Ajouter Informations personnelles
	function ajouter_infos_perso($prenom, $nom, $titre, $adresse, $pays, $ville, $email, $numtel, $age){
		$bdd = connexion_bdd();
		vider('infos_perso');
		try {
			$req = $bdd->prepare('INSERT INTO infos_perso VALUES(:prenom, :nom, :titre, :adresse, :pays, :ville, :email, :numtel, :age)');
			$req->execute(array(
					'prenom' => $prenom,
                    'nom' => $nom,
                    'titre' => $titre,
                    'adresse' => $adresse,
                    'pays' => $pays,
                    'ville' => $ville,
                    'email' => $email,
                    'numtel' => $numtel,
                    'age' => $age
				));
		return $email;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
	}

	function ajouter_dip_form($annee_debut, $annee_fin, $duree, $nom_diplome, $lieu, $extras){
		$bdd = connexion_bdd();
		try {
			$req = $bdd->prepare('INSERT INTO formation VALUES(:annee_debut, :annee_fin, :duree, :nom_diplome, :lieu, :extras)');
			$req->execute(array(
					'annee_debut' => $annee_debut,
                    'annee_fin' => $annee_fin,
                    'duree' => $duree,
                    'nom_diplome' => $nom_diplome,
                    'lieu' => $lieu,
                    'extras' => $extras
				));
		return $nom_diplome;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
	}

	function ajouter_exp($mois_debut, $annee_debut, $mois_fin, $annee_fin, $duree, $type, $lieu, $description, $extras){
		$bdd = connexion_bdd();
		try {
			$req = $bdd->prepare('INSERT INTO experience VALUES(:mois_debut, :annee_debut, :mois_fin, :annee_fin, :duree, :type, :lieu, :description, :extras)');
			$req->execute(array(
					'mois_debut' => $mois_debut,
					'annee_debut' => $annee_debut,
					'mois_fin' => $mois_fin,
                    'annee_fin' => $annee_fin,
                    'duree' => $duree,
                    'type' => $type,
                    'lieu' => $lieu,
					'description' => $description,
                    'extras' => $extras
				));
		return $type;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
	}

	function ajouter_lang($nom, $description, $extras){
		$bdd = connexion_bdd();
		try {
			$req = $bdd->prepare('INSERT INTO langue VALUES (:nom, :description, :extras)');
			$req->execute(array(
					'nom' => $nom,
                    'description' => $description,
                    'extras' => $extras
				));
		return $nom;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
	}

	function ajouter_loisir($nom, $extras){
		$bdd = connexion_bdd();
		try {
			$req = $bdd->prepare('INSERT INTO loisir VALUES(:nom, :extras)');
			$req->execute(array(
					'nom' => $nom,
                    'extras' => $extras
				));
		return $nom;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
	}

	function init_db() {
		try {
			vider('infos_perso');
			vider('langue');
			vider('loisir');
			vider('formation');
			vider('experience');
		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
	}


	function recup_donnee($table){
		$bdd = connexion_bdd();
		try {
			$req = $bdd->query('SELECT * FROM '. $table);
			$resultSet = $req->fetchAll();
			return $resultSet;
			$req->closeCursor();
			
		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));			
		}
	}

?>